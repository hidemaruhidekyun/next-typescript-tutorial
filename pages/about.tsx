import React from 'react';
import Layout from '../components/MyLayout';

const About: React.FunctionComponent = () => <div>
    <Layout>
        <p>This is the about page</p>
    </Layout>
</div>

export default About;